=====================================
Documentation for Mailman Suite Setup
=====================================

This repo is home for the documentation about `Mailman Suite`_.

.. _`Mailman Suite`: http://docs.mailman3.org/en/latest/devsetup.html
